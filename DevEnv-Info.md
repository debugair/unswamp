# JQuery for TS
https://code.visualstudio.com/docs/typescript/typescript-compiling
```
npm install -g typescript
npm install --save @types/jquery
```

# bandit
```
bandit -r .\unswamp\ -f html -o bandit.html
```

# unittest
```
python -m unittest discover tests
```

# pyment
```
cd D:\Code\GitLab\unswamp\_additional\docs
pyment .\..\..\unswamp\
```