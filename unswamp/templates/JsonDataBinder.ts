import $ = require("jquery");

export class JsonDataBinder {
    data: any;
    docType: RenderType;
    toc: any[] = [];
    max_level: number = 5;
    min_level: number = 4;

    constructor(data: any) {
        this.data = data;
        this.docType = this.getRenderType(data);
        if (this.docType == RenderType.CheckSuite) {
            this.bindCheckSuite(data, this.docType);
        }
        else if (this.docType == RenderType.CheckRun) {
            this.bindCheckRun(data, this.docType)
        }
    }

    bindCheckRun(data: any, type: RenderType): void {
        this.bindSuccess(data);
        var type_str = RenderType[type];
        document.title = "Docs '" + data.id + "' " + type_str;
        var info = {
            "Id": data.id,
            "Run Name": data.name,
            "Dataset Name": data.dataset_name,
            "{un}swamp version": data._unswamp_version
        }
        this.bindInformation(info, type_str);
        this.bindResultTable(data);
        this.bindStatsTable(data);
        this.bindMetaData(data.meta_data);
        this.bindToc(this.toc, this.max_level, this.min_level);
    }

    bindCheckSuite(data: any, type: RenderType): void {
        var type_str = RenderType[type];
        document.title = "Docs '" + data.id + "' " + type_str;
        this.appendHeading(data.id, 3);
        var info = {
            "Id": data.id,
            "Dataset Name": data.dataset_name,
            "{un}swamp version": data._unswamp_version
        }
        this.bindInformation(info, type_str);
        this.bindMetaData(data.meta_data);
        this.bindTableChecks(data);
        this.bindColumnChecks(data);
        this.bindCustomChecks(data);
        this.bindToc(this.toc, this.max_level, this.min_level);
    }

    bindSuccess(data: any): void {
        var state = "success";
        var rate = Math.round(data.pass_rate * 10000) / 100;
        var passed_checks = Math.round(data.pass_rate * data.total_checks);
        var failed_checks = data.total_checks - passed_checks;
        if (data.pass_rate != 1) {
            state = "danger";
        }
        var html = '<div class="alert alert-dark" role="alert"><span class="badge bg-' + state + '">' + rate + '%</span> success rate, <span class="badge bg-light text-dark">' + passed_checks + '</span> out of <span class="badge bg-light text-dark">' + data.total_checks + '</span> checks passed, <span class="badge bg-light text-dark">' + failed_checks + '</span> failed!</div>';
        this.appendMain(html);
    }

    bindInformation(info: any, type: string): void {
        this.appendHeading("Information", 4);
        this.appendHeading(type, 5);
        var tbl = this.getKvpTable(info);
        this.appendMain(tbl);
    }

    bindMetaData(meta_data: any): void {
        var tbl = this.getKvpTable(meta_data);
        if(tbl != ""){
            this.appendHeading("Meta Data", 5);
            this.appendMain(tbl);
        }
    }

    bindResultTable(data: any): void {
        this.appendHeading("Check Results", 4);
        var html = '<table class="table table-bordered table-striped table-xs">';
        html += '<thead><tr><th scope="col">State</th><th scope="col">Check Id</th><th scope="col">Message</th><th>Level</th></tr></thead>';
        html += '<tbody>';
        var results = data.results.sort(function (a, b) { return a.passed ? 1 : -1; });
        for (var pos in results) {
            var result = results[pos];
            var passed_class = 'bg-danger';
            var passed_label = 'NOK';
            if (result.passed) {
                passed_class = 'bg-success';
                passed_label = 'OK';
            }
            html += '<tr><td><span class="badge ' + passed_class + '">' + passed_label + '</span></td><td>' + result.check.id + '</td><td>' + result.message + '</td><td>' + result.check.level + '</td></tr>';
        }
        html += "</tbody></table>";
        this.appendMain(html);
    }

    bindTableChecks(data: any): void {
        var checks = data.checks.filter((c: { level: string; }) => c.level == "table")
        if (checks.length == 0) {
            return;
        }

        this.appendHeading("Table Checks", 4);
        checks = checks.sort((a: any, b: any) => a.id > b.id ? 1 : -1);
        this.appendColumnCheckAccordion(checks, "table_checks");
    }

    bindStatsTable(data: any): void {
        this.appendHeading("Run Statistics", 4);
        var stats = {
            "Dataset Rows": data.dataset_rows,
            "Dataset Columns": data.dataset_columns,
            "Start": data.start,
            "End": data.end,
        }
        var tbl = this.getKvpTable(stats);
        this.appendMain(tbl);
    }

    bindColumnChecks(data: any): void {
        var checks = data.checks.filter((c: { level: string; }) => c.level == "column")
        if (checks.length == 0) {
            return;
        }

        this.appendHeading("Column Level Checks", 4);

        checks = checks.sort((a: any, b: any) => a.arguments["column"] > b.arguments["column"] ? 1 : -1);
        var lastCol = "";
        var collChecks = [];

        for (var pos in checks) {
            var check = checks[pos]
            var currCol = check.arguments["column"];

            if (lastCol != currCol) {
                if (lastCol != "") {
                    this.appendColumnCheckAccordion(collChecks, lastCol, true);
                    collChecks = [];
                }
                lastCol = currCol;
            }
            collChecks.push(check);
        }
        this.appendColumnCheckAccordion(collChecks, lastCol, true);
    }

    bindCustomChecks(data: any): void {
        var checks = data.checks.filter((c: { level: string; }) => c.level == "custom")
        if (checks.length == 0) {
            return;
        }

        this.appendHeading("Custom Checks", 4);
        checks = checks.sort((a: any, b: any) => a.id > b.id ? 1 : -1);
        this.appendColumnCheckAccordion(checks, "custom_checks");
    }

    bindToc(toc: any[], max_level: number, min_level: number): void {
        var html = "";
        var last_level = 0;
        for (var pos in toc) {
            var val = toc[pos];
            if (val.level <= max_level && val.level >= min_level) {
                if (last_level < val.level) {
                    html += "<ul>";
                }
                else if (last_level > val.level) {
                    html += "</ul>";

                }
                html += '<li><a href="#' + val.id + '">' + val.heading + '</a></li>';
                last_level = val.level;
            }
        }
        html += "</ul>";
        $("#toc").append(html);
    }

    getRenderType(data: any): RenderType {
        if (data.unswamp_type == "unswamp.objects.core.CheckSuite.CheckSuite") {
            return RenderType.CheckSuite;
        }
        else if (data.unswamp_type == "unswamp.objects.core.CheckRun.CheckRun") {
            return RenderType.CheckRun;
        }
    }

    appendMain(html: any): void {
        $("#content_main").append(html);
    }

    getJsonHtml(obj: any): string {
        var obj_json = JSON.stringify(obj, null, 4).trim();
        return '<div class="code-box"><pre class="my-0"><code class="fw-lighter">' + obj_json + '</code></pre></div>';
    }

    getKvpRow(key: string, value: any): string {
        var tmp_val = value
        if (typeof value === 'object' && value !== null && !Array.isArray(value)) {
            tmp_val = "{"
            for (var obj_key in value) {
                tmp_val += '"' + obj_key + '" : "' + value[obj_key] + '", '
            }
            tmp_val += "}"
        }
        return '<tr><th scope="row">' + key + '</th><td>' + tmp_val + '</td></tr>';
    }

    getKvpTable(items: any): string {
        var counter = 0;
        var html = '<table class="table table-striped table-sm table-bordered">';
        for (var key in items) {
            counter += 1;
            html += this.getKvpRow(key, items[key])
        }
        html += '</table>';
        if(counter == 0){
            return "";
        }
        return html;
    }

    getColumnCheckAccordion(checks: any, id_part: string): string {
        var html = '<div class="accordion" id="accordionExample_' + id_part + '">';
        for (var pos in checks) {
            var check = checks[pos];
            var checkId = id_part + '_' + pos;

            html += '<div class="accordion-item">';
            html += '<h2 class="accordion-header" id="heading' + checkId + '">';
            html += '<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse' + checkId + '" aria-expanded="false" aria-controls="collapse' + checkId + '">';
            this.getHeading(check.id, 5, 'heading' + checkId)
            html += check.id;
            html += '</button>';
            html += '</h2>';
            html += '<div id="collapse' + checkId + '" class="accordion-collapse collapse" aria-labelledby="heading' + checkId + '" data-bs-parent="#accordionExample_' + id_part + '">';
            html += '<div class="accordion-body">';
            html += this.getCheckContent(check);
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }
        html += "</div>";
        return html;
    }

    appendColumnCheckAccordion(colChecks: any, colName: string, heading: boolean = false): void {
        var html = '<div class="py-2">';
        if (heading) {
            html += this.getHeading("Column " + colName, 5);
        }
        html += this.getColumnCheckAccordion(colChecks, colName);
        html += '</div>'
        this.appendMain(html);
    }

    getCheckContent(check: any): string {
        var info = {
            "Id": check.id,
            "Type": check.unswamp_type,
            "Description": check.description,
            "Details": "ToDo",
        }
        var html = this.getHeading("Information", 6);
        html += this.getKvpTable(info);
        html += this.getHeading("Meta Data", 6);
        html += this.getKvpTable(check.meta_data);
        html += this.getHeading("JSON", 6);
        html += this.getJsonHtml(check);
        return html;
    }

    getHeading(heading: string, level: number, id: string = null): string {
        var head_id = id;
        if (head_id == null) {
            head_id = this.getHeadId(heading);
        }
        this.toc.push({ "heading": heading, "level": level, "id": head_id });
        return '<h' + level + ' id="' + head_id + '">' + heading + '</h' + level + '>';
    }

    getHeadId(heading: string): string {
        var head_id = heading.toLowerCase();
        head_id = head_id.replace(" ", "");
        head_id = "heading_" + head_id;
        return head_id;
    }

    appendHeading(heading: string, level: number): void {
        heading = this.getHeading(heading, level);
        this.appendMain(heading);
    }
}

enum RenderType {
    Unknown = 0,
    CheckSuite = 1,
    CheckRun = 2
}

