from copy import copy
from unswamp.objects.checks.table.TableFunctionCheck import TableFunctionCheck


class CheckColumnsMatchOrderedList(TableFunctionCheck):
    def __init__(self, id, columns, active=True, meta_data=None):
        function_key = "table_check_columns_match_list_ordered"
        expectation_key = "result_eq_expectation"
        arguments = {"columns": columns, "expectation": copy(columns)}

        TableFunctionCheck.__init__(
            self, id, function_key, expectation_key, arguments, active, meta_data)