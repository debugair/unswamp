from unswamp.objects.checks.table.TableFunctionCheck import TableFunctionCheck


class CheckColumnsExists(TableFunctionCheck):
    def __init__(self, id, columns, active=True, meta_data=None):
        function_key = "table_check_columns_missing"
        expectation_key = "result_len_eq_expectation"
        arguments = {"columns": columns, "expectation": 0}

        TableFunctionCheck.__init__(
            self, id, function_key, expectation_key, arguments, active, meta_data)
