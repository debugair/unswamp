from unswamp.objects.checks.table.TableFunctionCheck import TableFunctionCheck


class CheckColumnsCountExact(TableFunctionCheck):
    def __init__(self, id, number_of_columns, active=True, meta_data=None):
        function_key = "table_check_columns_count"
        expectation_key = "result_eq_expectation"
        arguments = {"expectation": number_of_columns}

        TableFunctionCheck.__init__(
            self, id, function_key, expectation_key, arguments, active, meta_data)
