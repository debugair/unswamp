from unswamp.objects.checks.table.TableFunctionCheck import TableFunctionCheck


class CheckColumnsCountBetween(TableFunctionCheck):
    def __init__(self, id, number_of_columns_min, number_of_columns_max, inclusive=True, active=True, meta_data=None):
        function_key = "table_check_columns_count"
        expectation_key = "result_between_inclusive_expectations"
        if not inclusive:
            expectation_key = "result_between_exclusive_expectations"
        arguments = {"expectation_min": number_of_columns_min,
                     "expectation_max": number_of_columns_max}

        TableFunctionCheck.__init__(
            self, id, function_key, expectation_key, arguments, active, meta_data)
